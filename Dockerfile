FROM microsoft/dotnet:latest

USER 1001

COPY . /app

WORKDIR /app

RUN ["dotnet", "restore"]

RUN ["dotnet", "build"]

#EXPOSE 5000/tcp
EXPOSE 5000

CMD ["dotnet", "run", "--server.urls", "http://*:5000"]
